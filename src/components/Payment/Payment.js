import React from "react";
// const imgs = require.context("../../images/", true, /\.png$/);

export class Payment extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { provider, sms, gift, choosed, onClick } = this.props;
    const isChoosed = choosed === true;
    const isntChoosed = choosed === false;
    return (
      <div
        className={
          "Payment" +
          (isChoosed ? " choosed" : isntChoosed ? " not-choosed" : "")
        }
        onClick={onClick}
      >
        <div />
        {provider ? (
          <img
            src={require(`../../images/sprite.payment-${provider +
              (isntChoosed ? "-bw" : "")}.png`)}
          />
        ) : sms ? (
          <div>
            SMS<div>Только для России</div>
          </div>
        ) : gift ? (
          <div>
            Подарочный<div>код</div>
          </div>
        ) : null}
      </div>
    );
  }
}
