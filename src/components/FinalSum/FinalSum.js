import React from "react";

export class FinalSum extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const { useSale, months, rate, continuousSubscription } = this.props;
    const sum = months * rate;
    const year = months / 12;

    return (
      <div className="FinalSum">
        <div className="text">
          Итого к оплате (за{" "}
          {year === 1
            ? "1 год"
            : year > 1 ? year + " года" : months + " месяцев"})
        </div>
        {useSale ? (
          <div className="sum">
            <span className="grey">{sum} + 150</span> = {sum + 150} руб.
          </div>
        ) : (
          <div className="sum">{sum} руб.</div>
        )}
        {continuousSubscription ? (
          <div className="continuousSubscription">Далее 120 руб. месяц</div>
        ) : null}
      </div>
    );
  }
}
