import React from "react";

export class DurationSelector extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="DurationSelector">
        <div className="text">На какой срок</div>
        <div className="container">{this.props.children}</div>
      </div>
    );
  }
}
