import React from "react";

export class Widget extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="Widget">
        <div className="title">👑Клуб выгодных покупок</div>
        <div>{this.props.children}</div>{" "}
      </div>
    );
  }
}
