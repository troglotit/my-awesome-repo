import React from "react";

export class PaymentSelector extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="PaymentSelector">
        <div className="text">Выберите способ оплаты</div>
        <div className="container">{this.props.children}</div>
      </div>
    );
  }
}
